#!/usr/bin/env python

import logging
import datetime
import threading
import httplib
import socket
import shelve
import atexit

log = logging.getLogger(__name__)


class BaseBackend(object):

    def __init__(self):
        pass

    def _get(self, key):
        raise NotImplementedError

    def _set(self, key, data, meta, timestamp):
        raise NotImplementedError

    def _del(self, key):
        raise NotImplementedError

    def _keys(self):
        raise NotImplementedError

    def get(self, key):
        return self._get(key)

    def set(self, key, data, meta, timestamp=None):
        if not timestamp:
            timestamp = datetime.datetime.now()
        return self._set(key, data, meta, timestamp)

    def delete(self, key):
        return self._del(key)

    def keys(self, pattern=''):
        keys_list = []
        for key in self._keys():
            if key.startswith(pattern):
                keys_list.append(key)
        return keys_list

    def __getitem__(self, key):
        return self.get(key)

    def __setitem__(self, key, value):
        return self.set(key, *value)


class DictBackend(BaseBackend):

    def __init__(self):
        super(DictBackend, self).__init__()
        self.data = {}

    def _get(self, key):
        return self.data[key]

    def _set(self, key, data, meta, timestamp):
        self.data[key] = (data, meta, timestamp)

    def _del(self, key):
        del self.data[key]

    def _keys(self):
        return self.data.keys()


class ShelveBackend(BaseBackend):

    def __init__(self, filename):
        super(ShelveBackend, self).__init__()
        self.data = shelve.open(filename)
        atexit.register(self.close)

    def _get(self, key):
        return self.data[key]

    def _set(self, key, data, meta, timestamp):
        self.data[key] = (data, meta, timestamp)

    def _del(self, key):
        del self.data[key]

    def _keys(self):
        return self.data.keys()

    def close(self):
        self.data.close()


class RESTServerApp(object):

    def __init__(self, backend=None):
        if backend == None:
            backend = DictBackend()
        self.backend = backend

    weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    monthname = [None,
                 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    def date_time_string(self, timestamp=None):
        """Return the current date and time formatted for a message header."""
        year, month, day, hh, mm, ss, wd, y, z = timestamp.timetuple()
        s = "%s, %02d %3s %4d %02d:%02d:%02d GMT" % (
                self.weekdayname[wd],
                day, self.monthname[month], year,
                hh, mm, ss)
        return s

    def get_headers(self, meta, timestamp):
        headers = [
           ("Last-Modified", self.date_time_string(timestamp))
        ]
        if 'Content-Type' in meta:
            headers.append(('Content-Type', meta['Content-Type']))

        if 'Content-Type' in meta:
            headers.append(('Content-Length', meta['Content-Length']))

        return headers

    def get(self, path, environ, start_response):
        try:
            data, meta, timestamp = self.backend[path]

            headers = self.get_headers(meta, timestamp)

            start_response("200 OK", headers)
            return data
        except KeyError:
            start_response("404 NOT FOUND", [])
            return ["%s=%s\n" % (k, v) for k, v in sorted(environ.iteritems())]

    def head(self, path, environ, start_response):
        self.get(path, environ, start_response)
        return []

    def put(self, path, environ, start_response):
        length = environ['CONTENT_LENGTH']
        data = environ['wsgi.input'].read(int(length))
        meta = {
            'Content-Length': length,
            'Content-Type': environ['CONTENT_TYPE'],
        }
        self.backend.set(path, data, meta)
        start_response("201 CREATED", [('Location', path)])
        return data

    def post(self, path, environ, start_response):
        length = environ['CONTENT_LENGTH']
        data = environ['wsgi.input'].read(int(length))
        meta = {
            'Content-Length': length,
            'Content-Type': environ['CONTENT_TYPE'],
        }
        self.backend.set(path, data, meta)
        start_response("201 CREATED", [('Location', path)])
        return data

    def delete(self, path, environ, start_response):
        try:
            self.backend.delete(path)
            start_response("200 DELETED", [])
            return []
        except KeyError:
            start_response("404 NOT FOUND", [])
            return ["%s=%s\n" % (k, v) for k, v in sorted(environ.iteritems())]

    def __call__(self, environ, start_response):

        method = environ['REQUEST_METHOD']
        uri = environ['PATH_INFO']

        if method == "GET":
            return self.get(uri, environ, start_response)
        if method == "HEAD":
            return self.head(uri, environ, start_response)
        elif method == "POST":
            return self.post(uri, environ, start_response)
        elif method == "PUT":
            return self.put(uri, environ, start_response)
        elif method == "DELETE":
            return self.delete(uri, environ, start_response)
        else:
            start_response("400 BAD REQUEST", [])
            return ["%s=%s\n" % (k, v) for k, v in sorted(environ.iteritems())]


def get_test_server(addr, port, backend=None):

    from wsgiref.simple_server import make_server, \
                    WSGIRequestHandler, WSGIServer

    class LoggingWSGIRequestHandler(WSGIRequestHandler):

        def log_message(self, format, *args):
            log.info("%s - - [%s] %s" % \
                    (self.address_string(),
                     self.log_date_time_string(),
                     format % args))

    class StoppableHttpServer(WSGIServer):
        """http server that reacts to self.stop flag"""

        def serve_forever(self):
            """Handle one request at a time until stopped."""
            self.stop = False
            try:
                while not self.stop:
                    self.handle_request()
            except socket.error:
                pass
            self.server_close()

        def stop_server(self):
            log.info("Stopping testing server")

            #if sys.version_info > (2,5):
            #   self.shutdown()

            if not self.stop:
                self.stop = True
                conn = httplib.HTTPConnection("localhost:%d" % \
                                            self.server_port)
                conn.request("GET", "/")
                try:
                    conn.getresponse()
                except socket.error:
                    pass
                finally:
                    conn.close()

    app = RESTServerApp(backend)

    return make_server(addr, port, app, \
                    handler_class=LoggingWSGIRequestHandler,
                                       server_class=StoppableHttpServer)


def start_test_server(addr, port, backend=None):

    httpd = get_test_server(addr, port, backend)

    def run():
        httpd.serve_forever()

    thread = threading.Thread(target=run)
    thread.setDaemon(True)
    thread.start()

    httpd.thread = thread

    return httpd

if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)

    httpd = get_test_server('', 8888)
    log.info("Serving HTTP on port 8888...")

    # Respond to requests until process is killed
    httpd.serve_forever()
