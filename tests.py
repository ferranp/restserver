import os
import datetime
import restserver
import unittest
from restclient import Resource
from restclient.errors import ResourceNotFound, RequestFailed

PORT = 12233
ADDR = 'localhost'


class TestRestServer(unittest.TestCase):
    def setUp(self):

        self.httpd = restserver.start_test_server(ADDR, PORT)
        self.backend = self.httpd.get_app().backend
        self.client = Resource("http://%s:%d/" % (ADDR, PORT))

    def tearDown(self):
        self.httpd.stop_server()

    def test_404(self):
        self.assertRaises(ResourceNotFound, self.client.get, ("/aaa",))

    def test_put_and_get(self):
        DATA = 'Some data'
        self.client.put("/aaa", DATA)
        resp = self.client.get("/aaa")
        self.assertEqual(DATA, resp)

        self.client.put("/aaa2", DATA)
        self.client.put("/ccaaa", DATA)

        self.backend["/aaa3"] = ("data", tuple(), datetime.datetime.now())

        self.assertEqual(len(self.backend.keys()), 4)
        self.assertEqual(len(self.backend.keys("/aa")), 3)

        # get without headers
        resp = self.client.get("/aaa3")
        self.assertEqual("data", resp)

    def test_put_and_head(self):
        DATA = 'Some data'
        self.client.put("/aaa", DATA)
        resp = self.client.head("/aaa")
        self.assertEqual('', resp)

    def test_post_and_delete(self):
        DATA = 'Some data'
        self.client.post("/aaa", DATA)
        self.client.delete("/aaa")

        self.assertRaises(ResourceNotFound, self.client.get, ("/aaa",))

    def test_bad_method(self):
        #self.client.request("XXXX","/aaa")
        try:
            self.client.request("XXXX", "/aaa")
        except Exception, e:
            self.assertEqual(type(e), RequestFailed)
        #self.assertRaises(Exception,self.client.request,("XXXX","/aaa"))


class TestShelveServer(TestRestServer):
    def setUp(self):
        self.backend = restserver.ShelveBackend("test.shelve")

        self.httpd = restserver.start_test_server(ADDR, PORT,\
                    backend=self.backend)
        self.client = Resource("http://%s:%d/" % (ADDR, PORT))

    def tearDown(self):
        self.httpd.stop_server()
        self.backend.close()
        os.unlink("test.shelve")

if __name__ == '__main__':
    unittest.main()
