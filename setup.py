#!/usr/bin/env python

from distutils.core import setup

bitbucket_url = 'http://www.bitbucket.org/ferran/restserver/'
long_desc = open('README').read()

VERSION = '0.1'

setup(
    name='restserver',
    version=VERSION,
    description='A simple REST server for storing ' +
                    'resources and test REST clients',
    long_description=long_desc,
    author='Ferran Pegueroles Forcadell',
    author_email='ferran@pegueroles.com',
    url=bitbucket_url,
    download_url='%sdownloads/restserver-%s.tar.gz' % (bitbucket_url, VERSION),
    license='BSD',
    py_modules=['restserver'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Topic :: Software Development :: Libraries :: Python Modules'
        'Topic :: Software Development :: Libraries :: Python Modules'
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        'Topic :: Software Development :: Testing',
    ],
)
